import { SfdxCommand } from "@salesforce/command";
import { Messages } from "@salesforce/core";
import { AnyJson } from "@salesforce/ts-types";
import * as React from "react";
import { useRef, useEffect } from "react";
import * as blessed from "blessed";
import { render } from "react-blessed";
import * as execa from "execa";
import { AnsiStripStream } from "../../lib/transformPipes";
const ansiStripStream = new AnsiStripStream();
import * as readline from "readline";

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages("7summits-buildtool", "fed");

export default class Fed extends SfdxCommand {
  public static description = messages.getMessage("commandDescription");

  public static examples = [`$ sfdx x7s-watch:fed`];

  public static requiresProject = true;

  public lwcLogs = [];
  public auraLogs = [];
  private screen;

  public async run(): Promise<AnyJson> {
    // Rendering the React app using our screen
    this.runGulp();
    this.screen = blessed.screen({
      autoPadding: true,
      smartCSR: true,
      title: "7Summits Buildtool",
    });
    this.screen.key(["escape", "q", "C-c"], function (ch, key) {
      return process.exit(0);
    });
    this.renderScreen();
    // Return an object to be displayed with --json
    return {};
  }

  private async runGulp(): Promise<void> {
    const gulpLwc = execa("gulp", { preferLocal: true });
    gulpLwc.stdout.pipe(ansiStripStream);
    gulpLwc.stderr.pipe(process.stderr);
    let rl = readline.createInterface({
      input: ansiStripStream,
    });
    rl.on("line", (line) => {
      this.addToLogs(line);
    });
    gulpLwc.catch();
  }

  private renderScreen() {
    render(
      <Dashboard auraLogs={this.auraLogs} lwcLogs={this.lwcLogs} />,
      this.screen
    );
  }

  private addToLogs(log) {
    if (log.includes("Aura LESS")) {
      let tempLogs = [...this.auraLogs];
      if (tempLogs.length >= 50) {
        tempLogs.shift();
      }
      tempLogs.push(log);
      this.auraLogs = tempLogs;
    }
    if (log.includes("LWC LESS")) {
      let tempLogs = [...this.lwcLogs];
      if (tempLogs.length >= 50) {
        tempLogs.shift();
      }
      tempLogs.push(log);
      this.lwcLogs = tempLogs;
    }
    this.renderScreen();
  }
}

/**
 * Stylesheet
 */
const stylesheet = {
  bordered: {
    border: {
      type: "line",
    },
    style: {
      border: {
        fg: "blue",
      },
    },
  },
};

const Dashboard = (props) => {
  const { auraLogs, lwcLogs } = props;

  return (
    <element>
      <LwcLog logs={lwcLogs} />
      <AuraLog logs={auraLogs} />
      <Deployments />
    </element>
  );
};

const AuraLog = (props) => {
  const { logs } = props;
  const box = useRef(null);
  useEffect(() => {
    if (box.current != null) {
      box.current.setScrollPerc(100);
    }
  });
  return (
    <box
      label="Aura Component Gulp Log"
      class={stylesheet.bordered}
      width="75%"
      height="50%"
      top="50%"
      scrollable={true}
      alwaysScroll={true}
      ref={box}
    >
      {logs.join("\n")}
    </box>
  );
};

const LwcLog = (props) => {
  const { logs } = props;
  const box = useRef(null);
  useEffect(() => {
    if (box.current != null) {
      box.current.setScrollPerc(100);
    }
  });
  return (
    <box
      label="LWC Component Gulp Log"
      class={stylesheet.bordered}
      width="75%"
      height="50%"
      scrollable={true}
      alwaysScroll={true}
      ref={box}
    >
      {logs.join("\n")}
    </box>
  );
};

const Deployments = () => {
  const box = useRef(null);
  useEffect(() => {
    if (box.current != null) {
      box.current.setScrollPerc(100);
    }
  });
  return (
    <box
      label="SFDX Deployments"
      class={stylesheet.bordered}
      width="25%"
      height="100%"
      left="75%"
      scrollable={true}
      alwaysScroll={true}
      ref={box}
    ></box>
  );
};
