import { flags, SfdxCommand } from "@salesforce/command";
import { Messages } from "@salesforce/core";
import { AnyJson } from "@salesforce/ts-types";
import * as path from "path";
import * as execa from "execa";
import * as Listr from "listr";
import simpleGit, { SimpleGit } from "simple-git";
import cli from "cli-ux";

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages("7summits-buildtool", "create");

export default class Create extends SfdxCommand {
  public static description = messages.getMessage("commandDescription");

  public static examples = [
    `$ sfdx x7s-build:project:create --name example-project`,
    `$ sfdx x7s-build:project:create --name example-project --path example/directory`,
  ];

  protected static flagsConfig = {
    name: flags.string({
      char: "n",
      description: messages.getMessage("nameFlagDescription"),
      required: true,
    }),
    path: flags.filepath({
      char: "p",
      description: messages.getMessage("pathFlagDescription"),
      default: ".",
    }),
  };

  private get absolutePath(): string {
    return path.normalize(path.resolve(this.flags.path, this.flags.name));
  }

  public async run(): Promise<AnyJson> {
    // Get bitbucket username for recipes repo clone
    const bitbucketUsername = await cli.prompt(
      "What is your Bitbucket username?"
    );

    // Task list
    const tasks = new Listr([
      {
        title: "Create default SFDX project",
        task: () => this.createDefaultProject(),
      },
      {
        title: "Setup styles",
        task: () => {
          return new Listr([
            {
              title: "Clone recipes repo",
              task: () => this.cloneRecipesRepo(bitbucketUsername),
            },
            {
              title: "Copy styles directory",
              task: () => this.copyStylesDirectory(),
            },
            {
              title: "Copy gulpfile",
              task: () => this.copyGulpfile(),
            },
            {
              title: "Add gulp dependencies",
              task: () => this.addGulp(),
            },
            {
              title: "Remove local recipes repo",
              task: () => this.removeRecipesRepo(),
            },
          ]);
        },
      },
      {
        title: "Run npm install",
        task: () => this.npmInstall(),
      },
      {
        title: "Init git repo",
        task: () => this.initGit(),
      },
    ]);

    tasks.run().catch((err) => {
      console.error(err);
    });

    // Return an object to be displayed with --json
    return {};
  }

  // Create default SFDX project using SFDX CLI
  private async createDefaultProject(): Promise<Object> {
    return execa("sfdx", ["force:project:create", "-n", this.flags.name], {
      cwd: this.flags.path,
    });
  }

  // Clone internal Lightning Component Recipes repo
  private async cloneRecipesRepo(bitbucketUsername: string): Promise<Object> {
    const git: SimpleGit = simpleGit();
    return git.clone(
      `https://${bitbucketUsername}@bitbucket.org/7Summits/7summits-lightning-component-recipes.git`,
      `${this.absolutePath}/.tempRepo/`,
      {
        "--depth": 1,
        "--branch": "master",
        "--single-branch": null,
      }
    );
  }

  // Copy styles from cloned recipes repo to root of project file
  private async copyStylesDirectory(): Promise<Object> {
    return execa("cp", ["-a", ".tempRepo/styles", "."], {
      cwd: this.absolutePath,
    });
  }

  // Copy gulpfile from cloned recipes repo to root of project file
  private async copyGulpfile(): Promise<Object> {
    return execa("cp", ["-a", ".tempRepo/gulpfile.js", "."], {
      cwd: this.absolutePath,
    });
  }

  // Get gulp dependencies from recipes repo
  private async getGulpDependencies(): Promise<Object> {
    const { stdout } = await execa("cat", [".tempRepo/package.json"], {
      cwd: this.absolutePath,
    });
    const parsedPackage = JSON.parse(stdout);
    const devDeps = parsedPackage.devDependencies;
    return Object.keys(devDeps)
      .filter(function (k) {
        return k.indexOf("gulp") == 0;
      })
      .reduce(function (newData, k) {
        newData[k] = devDeps[k];
        return newData;
      }, {});
  }

  // Add gulp dependencies to project
  private async addGulp(): Promise<Object> {
    const gulpDeps = await this.getGulpDependencies();
    const { stdout } = await execa("cat", ["package.json"], {
      cwd: this.absolutePath,
    });
    const parsedPackage = JSON.parse(stdout);
    const devDeps = {
      ...parsedPackage.devDependencies,
      ...gulpDeps,
    };
    parsedPackage.devDependencies = devDeps;
    return execa(
      `echo '${JSON.stringify(parsedPackage, null, 2)}' > package.json`,
      {
        shell: true,
        cwd: this.absolutePath,
      }
    );
  }

  // Remove local recipes repo
  private async removeRecipesRepo(): Promise<Object> {
    return execa("rm", ["-rf", ".tempRepo/"], {
      cwd: this.absolutePath,
    });
  }

  // Run npm install
  private async npmInstall(): Promise<Object> {
    return execa("npm", ["install"], { cwd: this.absolutePath });
  }

  // Initialize git repo
  private async initGit(): Promise<Object> {
    return execa("git init && git add . && git commit -m 'Init'", {
      cwd: this.absolutePath,
      shell: true,
    });
  }
}
