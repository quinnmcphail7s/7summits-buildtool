import { Transform } from "stream";

export class AnsiStripStream extends Transform {
  constructor() {
    super();
  }

  _transform(chunk, enc, done) {
    const replaceAnsi = chunk
      .toString()
      .replace(
        /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
        ""
      );
    this.push(replaceAnsi);
    done();
  }
}
