# 7summits-buildtool

7summits Buildtool

[![Version](https://img.shields.io/npm/v/7summits-buildtool.svg)](https://npmjs.org/package/7summits-buildtool)
[![CircleCI](https://circleci.com/gh//7summits-buildtool/tree/master.svg?style=shield)](https://circleci.com/gh//7summits-buildtool/tree/master)
[![Appveyor CI](https://ci.appveyor.com/api/projects/status/github//7summits-buildtool?branch=master&svg=true)](https://ci.appveyor.com/project/heroku/7summits-buildtool/branch/master)
[![Codecov](https://codecov.io/gh//7summits-buildtool/branch/master/graph/badge.svg)](https://codecov.io/gh//7summits-buildtool)
[![Greenkeeper](https://badges.greenkeeper.io//7summits-buildtool.svg)](https://greenkeeper.io/)
[![Known Vulnerabilities](https://snyk.io/test/github//7summits-buildtool/badge.svg)](https://snyk.io/test/github//7summits-buildtool)
[![Downloads/week](https://img.shields.io/npm/dw/7summits-buildtool.svg)](https://npmjs.org/package/7summits-buildtool)
[![License](https://img.shields.io/npm/l/7summits-buildtool.svg)](https://github.com//7summits-buildtool/blob/master/package.json)

<!-- toc -->
* [7summits-buildtool](#7summits-buildtool)
* [Debugging your plugin](#debugging-your-plugin)
<!-- tocstop -->
  <!-- install -->
  <!-- usage -->
```sh-session
$ npm install -g 7summits-buildtool
$ sfdx COMMAND
running command...
$ sfdx (-v|--version|version)
7summits-buildtool/0.0.0 darwin-x64 node-v16.2.0
$ sfdx --help [COMMAND]
USAGE
  $ sfdx COMMAND
...
```
<!-- usagestop -->
<!-- commands -->
* [`sfdx x7s-build:project:create -n <string> [-p <filepath>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-x7s-buildprojectcreate--n-string--p-filepath---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx x7s-watch:fed [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-x7s-watchfed---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx x7s-build:project:create -n <string> [-p <filepath>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create an SFDX project file for 7Summits projects

```
create an SFDX project file for 7Summits projects

USAGE
  $ sfdx x7s-build:project:create -n <string> [-p <filepath>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -n, --name=name                                                                   (required) name of project

  -p, --path=path                                                                   [default: .] path of the project
                                                                                    file

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx x7s-build:project:create --name example-project
  $ sfdx x7s-build:project:create --name example-project --path example/directory
```

_See code: [lib/commands/x7s-build/project/create.js](https://github.com/quinnmcphail/7summits-buildtool/blob/v0.0.0/lib/commands/x7s-build/project/create.js)_

## `sfdx x7s-watch:fed [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

watch SFDX project folder for FED changes

```
watch SFDX project folder for FED changes

USAGE
  $ sfdx x7s-watch:fed [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  $ sfdx x7s-watch:fed
```

_See code: [lib/commands/x7s-watch/fed.js](https://github.com/quinnmcphail/7summits-buildtool/blob/v0.0.0/lib/commands/x7s-watch/fed.js)_
<!-- commandsstop -->
<!-- debugging-your-plugin -->

# Debugging your plugin

We recommend using the Visual Studio Code (VS Code) IDE for your plugin development. Included in the `.vscode` directory of this plugin is a `launch.json` config file, which allows you to attach a debugger to the node process when running your commands.

To debug the `hello:org` command:

1. Start the inspector

If you linked your plugin to the sfdx cli, call your command with the `dev-suspend` switch:

```sh-session
$ sfdx hello:org -u myOrg@example.com --dev-suspend
```

Alternatively, to call your command using the `bin/run` script, set the `NODE_OPTIONS` environment variable to `--inspect-brk` when starting the debugger:

```sh-session
$ NODE_OPTIONS=--inspect-brk bin/run hello:org -u myOrg@example.com
```

2. Set some breakpoints in your command code
3. Click on the Debug icon in the Activity Bar on the side of VS Code to open up the Debug view.
4. In the upper left hand corner of VS Code, verify that the "Attach to Remote" launch configuration has been chosen.
5. Hit the green play button to the left of the "Attach to Remote" launch configuration window. The debugger should now be suspended on the first line of the program.
6. Hit the green play button at the top middle of VS Code (this play button will be to the right of the play button that you clicked in step #5).
   <br><img src=".images/vscodeScreenshot.png" width="480" height="278"><br>
   Congrats, you are debugging!
